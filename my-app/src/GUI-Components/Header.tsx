import React from "react";
import styled from "styled-components";
import { Row } from "./LayoutHelpers/Row";
import { EngravedText } from "./Wooden/EngravedText";
import { Icon } from "./Icon";
import { Icons } from "../ApplicationConstants/Icons";
import { MAIL_ADRESS } from "../ApplicationConstants/BandMockData";
import { DEVICES } from "../ApplicationConstants/Breakpoints";

const Root = styled.div`
  z-index: 10;
  position: fixed;
  width: 100%;
  height: var(--header-height);
  padding: 10px;
  box-sizing: border-box;
  background: var(--bg-secondary);
  box-shadow: inset 0px 0px 0px 1px rgba(0, 0, 0, 0.2),
    inset 0px -2px 0px 0px rgba(0, 0, 0, 0.3),
    inset 0px 2px 0px 0px rgba(255, 255, 255, 0.5);
  border-bottom: 1px solid white;
  color: var(--text-secondary);
`;

const EngravedTextContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 5px;
`;

const StyledEngravedText = styled(EngravedText)`
  letter-spacing: 1px;
  font-size: 2em;
  color: var(--icon-color);
`;

const MailIcon = styled(Icon)`
  padding-left: 1em;
`;

const MailText = styled.p`
  position: absolute;
  right: 100%;
  top: 50%;
  transform: scaleX(0) translateY(-50%);
  font-size: 1.5em;
  background: var(--bg-primary);
  padding: 10px;
  transition: transform 0.3s;
  transform-origin: right;
  border: 1px solid white;
  @media screen and (min-width: ${DEVICES.PORTRAIT_TABLET}) {
    transform: scaleX(1) translateY(-50%);
  }
`;

const MailIconContainer = styled.div`
  position: relative;
  display: flex;
  cursor: pointer;
  &:hover ${MailIcon} {
    fill: var(--icon-color);
  }
  &:hover ${MailText} {
    transform: scaleX(1) translateY(-50%);
    color: var(--icon-color);
  }
`;

interface Props {}

export function Header(props: Props) {
  return (
    <Root>
      <Row height="100%" justify="space-between">
        <EngravedTextContainer>
          <StyledEngravedText>Willie Böck</StyledEngravedText>
        </EngravedTextContainer>
        <MailIconContainer>
          <MailIcon paths={Icons.MAIL_ICON}></MailIcon>
          <MailText>{MAIL_ADRESS}</MailText>
        </MailIconContainer>
      </Row>
    </Root>
  );
}
