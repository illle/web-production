import React from "react";
import styled from "styled-components";

const Root = styled.svg`
  ${(props) => `
    fill: ${props.color || "white"};
    stroke: ${props.color || "white"};
    width: ${props.width};
    height: ${props.height};
  `}

  display: inline-block;
  veritcal-align: middle;
  stroke-width: 0;

`;

const Path = styled.path``;

interface Props {
  paths: string | string[];
  size?: string;
  color?: string;
  //...
  selected?: boolean;
  
}

export function Icon(props: Props) {
  const { paths, ...rest } = props;

  return (
    <Root
      width={props.size || "32px"}
      height={props.size || "32px"}
      viewBox="0 0 1024 1024"
      {...rest}
    >
      {[].concat(paths).map((path, i) => (
        <Path key={i} d={path}></Path>
      ))}
    </Root>
  );
}
