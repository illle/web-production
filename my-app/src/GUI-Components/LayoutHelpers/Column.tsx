import React from "react";
import styled from "styled-components";
import { Container, ContainerProps } from './Container';

const Root = styled.div`
`;


export function Column(props: ContainerProps) {

  const {width, ...rest} = props;
  return (
    <Container width={props.width || "100%"} direction="column" {...rest}/>
  );
}
