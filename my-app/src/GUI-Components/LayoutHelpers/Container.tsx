import React, { useState, useEffect } from "react";
import styled from "styled-components";

const Root = styled.div`
  position: relative;
  display: flex;
  ${(props: any) => `
    flex-direction: ${props.direction || "initial"};
    width: ${props.width || "auto"};
    height: ${props.height || "auto"};
    justify-content: ${props.justify || "initial"};
    align-content: ${props.alignContent || "initial"};
  `}

`;

type Direction = "row" | "column";

type Justify =
  | "flex-start"
  | "flex-end"
  | "center"
  | "space-between"
  | "space-around"
  | "initial"
  | "inherit";

type AlignContent =
  | "flex-start"
  | "flex-end"
  | "center"
  | "space-between"
  | "space-around"
  | "stretch";

export interface ContainerProps {
  width?: string;
  height?: string;
  justify?: Justify;
  alignContent?: AlignContent;
  children?: JSX.Element | JSX.Element[];
}

interface Props extends ContainerProps {
  direction?: Direction;
}

export function Container(props: Props) {
  const [direction, setDirection] = useState<Direction>(
    props.direction || "row"
  );

  useEffect(() => {
    setDirection(props.direction || "row");
  }, [props.direction]);

  const { children, ...rest } = props;

  return (
    <Root direction={direction} {...rest}>
      {children}
    </Root>
  );
}
