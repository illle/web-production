import React from "react";
import { Container, ContainerProps } from './Container';

export function Row(props: ContainerProps) {

  const {width, ...rest} = props;
  return (
    <Container width={props.width || "100%"} direction="row" {...rest}/>
  );
}
