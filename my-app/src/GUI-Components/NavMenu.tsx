import React from "react";
import styled from "styled-components";
import { DEVICES } from "../ApplicationConstants/Breakpoints";
import { Icons } from "../ApplicationConstants/Icons";
import { Route } from "../ApplicationConstants/Route";
import { Icon } from "./Icon";
import { Row } from "./LayoutHelpers/Row";

const RouteIcon = styled(Icon)`
  ${(props: any) => `
  filter: ${
    props.selected ? "grayscale(0%) opacity(1)" : "grayscale(100%) opacity(0.7)"
  };
`}
  fill: #df49a6;

  transition: 0.4s;
  @media only screen and (min-width: ${DEVICES.LANDSCAPE_TABLET}) {
    width: 32px;
    height: 32px;
  }
`;

const ArrowIcon = styled(RouteIcon)`
  display: none;
  margin-left: 1.2em;
  margin-right: auto;
  margin-top: 2em;
  margin-bottom: 3em;
  transform: rotate(180deg);
  fill: white;

  @media only screen and (min-width: ${DEVICES.LANDSCAPE_TABLET}) {
    display: flex;
  }
`;

const RouteText = styled.a`
  padding: 3px;
  font-size: 1.5rem;
  color: var(--text-primary);
  display: none;
  margin-left: 0.3rem;

  @media screen and (min-width: ${DEVICES.PORTRAIT_TABLET}) and (max-width: ${DEVICES.LANDSCAPE_TABLET}) {
    display: inline;
  }
`;

const Root = styled.nav`
  position: fixed;
  bottom: 0;
  z-index: 10;
  width: 100%;
  height: 60px;
  background: var(--bg-secondary);
  padding: 5px;
  box-sizing: border-box;
  transition: width 0.3s;
  @media only screen and (min-width: ${DEVICES.LANDSCAPE_TABLET}) {
    height: 100%;
    top: var(--header-height);
    width: var(--nav-side-width);
    padding: 0;

    &:hover {
      width: 200px;

      & ${RouteText} {
        display: inline;
        margin: auto auto auto 1rem;
      }

      & ${ArrowIcon} {
        transform: rotate(0deg);
      }
    }
  }
  @media only screen and (min-width: ${DEVICES.LARGE_DESKTOP}) {
    display: none;
  }
`;

const RouteList = styled.ul`
  list-style: none;
  display: flex;
  align-items: flex-start;
  overflow: hidden;
  justify-content: space-between;
  margin: 0 15px 0 15px;

  > * {
    &:not(:last-child) {
      margin-right: 3px;
    }
  }

  @media only screen and (min-width: 768px) {
    flex-direction: column;
    height: 25%;
    margin-top: 2rem;
  }
`;

const RouteListItem = styled.li`
  padding: 5px;
  border-radius: 10%;
  cursor: pointer;
  &:hover ${RouteIcon} {
    filter: grayscale(0%);
  }
`;

interface Props {
  onRoute: Route;
  setRoute(route: Route);
}

export function NavMenu(props: Props) {
  const routes = () => {
    return Object.values(Route).map((route, i) => {
      return (
        <RouteListItem key={i} onClick={() => props.setRoute(route)}>
          <Row>
            <RouteIcon
              selected={route === props.onRoute}
              paths={pathForRoute(route)}
            />
            <RouteText>{route}</RouteText>
          </Row>
        </RouteListItem>
      );
    });
  };

  const pathForRoute = (route: Route) => {
    switch (route) {
      case Route.HOME:
        return Icons.HOME;
      case Route.TOUR:
        return Icons.AIRPLANE;
      case Route.LISTEN:
        return Icons.HEADPHONES;
    }
  };

  return (
    <Root>
      <ArrowIcon paths={Icons.LEFT_ARROW}></ArrowIcon>
      <RouteList>{routes()}</RouteList>
    </Root>
  );
}
