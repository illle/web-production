import React from "react";
import styled from 'styled-components'

const Root = styled.div`
  ${(props: any) => `
    color: ${props.color || "#3B5957"};
    font-size: ${props.fontSize || "16px"};
  `}
  font-weight: bold;
  font-family: "Century Gothic", "Helvetica", sans-serif;
  text-shadow: 0px 1px 0px rgba(255,255,255,.5);
`

interface Props {
  children: any;
  fontSize?: string;
  color?: string;
}

export function EngravedText(props: Props) {

  const {children, ...rest} = props;

  return <Root {...rest}>{children}</Root>;
}
