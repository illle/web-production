import React from "react";
import styled from "styled-components";

const Root = styled.div`
  box-shadow: inset 0px 0px 0px 1px rgba(0, 0, 0, 0.2),
    inset 0px -2px 0px 0px rgba(0, 0, 0, 0.3),
    inset 0px 2px 0px 0px rgba(255, 255, 255, 0.5);
  border-radius: 10px;
  margin: auto;
  background-color: #cc8800;
  background-image: url("https://www.transparenttextures.com/patterns/dark-wood.png");
  padding: 1rem;
`;
interface Props {
  children?: JSX.Element | JSX.Element[];
}

export function WoodenFrame(props: Props) {
  return <Root>{props.children}</Root>;
}
