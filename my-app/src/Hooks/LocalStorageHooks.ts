import { useState } from "react";

export function useLocalStorage(_: {
  itemName: string;
  fallback?: () => string;
}): [string, (newValue: string) => void] {
  let saved = localStorage.getItem(_.itemName);
  if (!saved && _.fallback) {
    saved = _.fallback();
    localStorage.setItem(_.itemName, saved);
  }
  const [state, setState] = useState(saved);
  const save = (newValue: string) => {
    localStorage.setItem(_.itemName, newValue);
    setState(newValue);
  };

  return [state, save];
}

interface Converter<T> {
  objToStr(obj: T): string;
  strToObj(str: string): T;
}

export function useLocalComplex<T>(_: {
  itemName: string;
  converter: Converter<T>;
  fallback?: () => T;
}): [T, (newValue: T) => void] {
  const [stringValue, saveStringValue] = useLocalStorage({
    itemName: _.itemName,
  });
  const save = (value: T) => {
    const converted = _.converter.objToStr(value);
    saveStringValue(converted);
  };
  const value = (str: string): T => {
    if (!str && _.fallback) {
      const value = _.fallback();
      saveStringValue(_.converter.objToStr(value));
      return value;
    } else {
      return _.converter.strToObj(str);
    }
  };

  return [value(stringValue), save];
}
