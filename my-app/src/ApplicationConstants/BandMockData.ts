/*
    Randomly generated from https://www.fantasynamegenerators.com/song-title-generator.php
*/
export const ALBUMS = {
  "Road for an Evening": [
    "Wind Of Infinity",
    "Strength Of My Ways",
    "Thinking Of My World",
    "Morning Song",
    "Aquatic Love",
    "Dearest, Hold My Hand",
    "Sing A Song With Me",
    "I Love I'm Sophisticated",
    "I Know He's Innocent",
  ],
  "Passion For The Mood": [
    "Days Of Mystery",
    "The Girl Of My Ways",
    "Crazy Of My Story",
    "Blissful Miracles",
    "Paradise Pleasures",
    "Darling, Thank You",
    "She Loves I Got The Moves",
    "He Thinks I Wrote You A Song",
  ],
  "Morning For Tomorrow": [
    "Heart Of My Friends",
    "Rhythm Of My Creation",
    "Young Chance",
    "Endless Choice",
    "Baby, I'm Coming Home",
    "I'm Glad You're Mine",
    "I Think I Love Her",
    "I Think He Ain't Bad",
  ]
};

export const BAND_MEMBERS = {
  "Mimmi": "Vocals",
  "Ingrid": "Vocals",
  "Moa": "Vocals",
  "Nisse": "Bass",
  "Axel": "Drums",
  "David": "Piano",
  "Olle": "Guitar",
  "Anton": "Trumpet",
  "Magnus": "Saxophone"
}

export const SHOWS = {
  "10-05-2020": "Amsterdam",
  "12-05-2020": "Berlin",
  "13-05-2020": "London",
  "15-05-2020": "Stockholm",
  "18-05-2020": "Oslo",
  "19-05-2020": "Copenhagen",
  "22-05-2020": "Paris",
  "25-05-2020": "Lisbon",
  "28-05-2020": "Madrid",
  "02-06-2020": "Rome",
  "05-06-2020": "Venice",
  "06-06-2020": "Istanbul",
  "08-06-2020": "Budapest",
  "12-06-2020": "Nuremberg",
}

export const MAIL_ADRESS = "band@williebock.com"
