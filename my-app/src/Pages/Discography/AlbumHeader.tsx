import React from "react";
import styled from "styled-components";
import { Row } from "../../GUI-Components/LayoutHelpers/Row";
import { Icons } from "../../ApplicationConstants/Icons";
import { Icon } from "../../GUI-Components/Icon";

const Root = styled(Row)`
  margin-bottom: 0.5em;
  padding-bottom: 0.5em;
  border-bottom: 1px solid white;
`;

const NameText = styled.h1`
  font-size: 24px;
  color: var(--text-primary);
`;

const ShoppingIcon = styled(Icon)`
  margin-right: 10px;
  cursor: pointer;
  &:hover {
    fill: var(--icon-color);
  }
`;

interface Props {
  name: string;
}

export function AlbumHeader(props: Props) {
  return (
    <Root justify="space-between">
      <NameText>{props.name}</NameText>
      <ShoppingIcon paths={Icons.SHOPPING_CART}></ShoppingIcon>
    </Root>
  );
}
