import React from "react";
import styled from "styled-components";
import { ALBUMS } from "../../ApplicationConstants/BandMockData";
import { AlbumContainer } from "./AlbumContainer";
import { DEVICES } from "../../ApplicationConstants/Breakpoints";

const PageHeight = styled.div`
  min-height: calc(100vh - var(--header-height) - var(--nav-height));
  max-height: calc(100vh - var(--header-height) - var(--nav-height));
  overflow-y: scroll;
  box-sizing: border-box;
  @media screen and (min-width: ${DEVICES.LANDSCAPE_TABLET}) {
    min-height: calc(100vh - var(--header-height));
    max-height: calc(100vh - var(--header-height));
  }
`;

const Root = styled(PageHeight)`
  padding: 1em;
`;

interface Props {}

export function DiscographyPage(props: Props) {
  return (
    <Root>
      {Object.entries(ALBUMS).map((entry, i) => {
        const albumName = entry[0];
        const songTitles = entry[1];
        return (
          <AlbumContainer
            key={i}
            albumName={albumName}
            songTitles={songTitles}
          />
        );
      })}
    </Root>
  );
}
