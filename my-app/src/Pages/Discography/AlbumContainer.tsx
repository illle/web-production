import React from "react";
import styled from "styled-components";
import { AlbumHeader } from "./AlbumHeader";
import { SongList } from "./SongList";

const Root = styled.div`
  margin-bottom: 1rem;
`;
interface Props {
  albumName: string;
  songTitles: string[];
}

export function AlbumContainer(props: Props) {
  return (
    <Root>
      <AlbumHeader name={props.albumName} />
      <SongList songTitles={props.songTitles}></SongList>
    </Root>
  );
}
