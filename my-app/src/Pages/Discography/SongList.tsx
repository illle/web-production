import React from "react";
import styled from "styled-components";
import { Icons } from "../../ApplicationConstants/Icons";
import { Icon } from "../../GUI-Components/Icon";
import { Row } from "../../GUI-Components/LayoutHelpers/Row";

const Root = styled.ul`
  > * {
    :not(:last-child) {
      margin-bottom: 0.5em;
    }
  }
`;
const ListItem = styled.li`
  width: 100%;
  position: relative;
`;

const SongTitleText = styled.h2`
  padding: 0.5em;
  font-size: 18px;
  color: var(--text-secondary);
`;

const StreamingRow = styled(Row)`
  position: absolute;
  padding: 5px;
  background: var(--bg-primary);
  width: auto;
  z-index: 10;
  right: 100%;
  padding: 10px;
  top: 50%;
  border: 1px solid white;
  transform: scaleX(0) translateY(-50%);
  filter: brightness(200%);
  transition: transform 0.3s;
  transform-origin: right;
  > * {
    :not(:last-child) {
      margin-right: 0.5em;
    }
  }
`;

const ListenIcon = styled(Icon)`
  margin: auto 5px auto 0;
  cursor: pointer;
`;

const IconWrapper = styled.div`
  position: relative;
  margin-right: 10px;
  display: flex;
  padding-left: 10px;
  &:hover ${StreamingRow} {
    transform: scaleX(1) translateY(-50%);
  }
  &:hover ${ListenIcon} {
    fill: var(--icon-color);
  }
`;

const StreamingLink = styled.a`
  cursor: pointer;
`;

const StreamingImg = styled.img`
  width: 32px;
`;

interface Props {
  songTitles: string[];
}

export function SongList(props: Props) {
  return (
    <Root>
      {props.songTitles.map((title, i) => {
        return (
          <ListItem key={i}>
            <Row justify="space-between" alignContent="center">
              <SongTitleText>{title}</SongTitleText>
              <IconWrapper>
                <ListenIcon size="24px" paths={Icons.HEADPHONES}></ListenIcon>
                <StreamingRow>
                  <StreamingLink>
                    <StreamingImg
                      src="Logos/Spotify.png"
                      alt="Spotify icon"
                    ></StreamingImg>
                  </StreamingLink>
                  <StreamingLink>
                    <StreamingImg
                      src="Logos/Tidal.png"
                      alt="Tidal icon"
                    ></StreamingImg>
                  </StreamingLink>
                  <StreamingLink>
                    <StreamingImg
                      src="Logos/Youtube.png"
                      alt="YouTube icon"
                    ></StreamingImg>
                  </StreamingLink>
                </StreamingRow>
              </IconWrapper>
            </Row>
          </ListItem>
        );
      })}
    </Root>
  );
}
