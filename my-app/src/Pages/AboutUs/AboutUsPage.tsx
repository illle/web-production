import React from "react";
import styled from "styled-components";
import { BAND_MEMBERS } from "../../ApplicationConstants/BandMockData";
import { DEVICES } from "../../ApplicationConstants/Breakpoints";
import { Row } from "../../GUI-Components/LayoutHelpers/Row";
import { WoodenFrame } from "../../GUI-Components/Wooden/WoodenFrame";

const PageHeight = styled.div`
  min-height: calc(100vh - var(--header-height) - var(--nav-height));
  max-height: calc(100vh - var(--header-height) - var(--nav-height));
  overflow-y: scroll;
  box-sizing: border-box;
  @media screen and (min-width: ${DEVICES.LANDSCAPE_TABLET}) {
    min-height: calc(100vh - var(--header-height));
    max-height: calc(100vh - var(--header-height));
  }
`;

const Root = styled(PageHeight)`
  color: var(--text-primary);
  padding: 1em;
  font-size: 1.5rem;
  display: flex;
  flex-direction: column;
  box-sizing: border-box;
`;

const Header = styled.h2`
  font-size: 1.5em;
  margin-bottom: 0.2em;
`;

const AboutUsText = styled.p`
  margin-bottom: 0.5em;
  font-size: 1em;
  white-space: pre-wrap;
  display: inline;
`;

const BandText = styled(AboutUsText)`
  color: var(--icon-color);
`;

const MembersList = styled.ul`
  margin-top: 1em;
  margin-bottom: 1em;

  > * {
    &:not(:last-child) {
      margin-bottom: 0.5em;
    }
  }
`;

const MembersListItem = styled.li`
  color: var(--text-secondary);
  letter-spacing: 2px;
`;

const BandImage = styled.img`
  max-width: 100%;
`;

const NameText = styled.p``;

const RoleText = styled.p``;

interface Props {}

export function AboutUsPage(props: Props) {
  return (
    <Root>
      <div>
        <Header>Hello there,</Header>
        <AboutUsText>we're </AboutUsText>
        <BandText>Willie Böck</BandText>
        <AboutUsText> - a funk band of nine people:</AboutUsText>
      </div>
      <MembersList>
        {Object.entries(BAND_MEMBERS).map((entry, i) => {
          const name = entry[0];
          const role = entry[1];
          return (
            <MembersListItem key={i}>
              <Row justify="space-between">
                <NameText>{name}</NameText>
                <RoleText>{role}</RoleText>
              </Row>
            </MembersListItem>
          );
        })}
      </MembersList>
      <WoodenFrame>
        <BandImage
          src="BandPicture.jpg"
          alt="Picture of Willie Böck"
        ></BandImage>
      </WoodenFrame>
    </Root>
  );
}
