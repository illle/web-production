import React from "react";
import styled from "styled-components";
import { SHOWS } from "../../ApplicationConstants/BandMockData";
import { DEVICES } from "../../ApplicationConstants/Breakpoints";
import { Icons } from "../../ApplicationConstants/Icons";
import { Icon } from "../../GUI-Components/Icon";
import { Row } from "../../GUI-Components/LayoutHelpers/Row";

const PageHeight = styled.div`
  min-height: calc(100vh - var(--header-height) - var(--nav-height));
  max-height: calc(100vh - var(--header-height) - var(--nav-height));
  overflow-y: scroll;
  box-sizing: border-box;
  @media screen and (min-width: ${DEVICES.LANDSCAPE_TABLET}) {
    min-height: calc(100vh - var(--header-height));
    max-height: calc(100vh - var(--header-height));
  }
`;

const Root = styled(PageHeight)`
  padding: 1rem;
  color: white;
  font-size: 1rem;
  letter-spacing: 2px;
  @media screen and (min-width: ${DEVICES.PORTRAIT_TABLET}) {
    font-size: 1.5rem;
  }
`;

const Header = styled.h2`
  font-size: 1.5em;
  margin-bottom: 0.5em;
`;

const ShowList = styled.ul`
  > * {
    &:not(:last-child) {
      margin-bottom: 0.5em;
    }
  }
`;

const ShowListItem = styled.li``;

const DateText = styled.p`
  width: 50%;
`;

const LocationText = styled.p``;

const TicketIcon = styled(Icon)``;

const TicketLink = styled.a`
  cursor: pointer;
  margin-left: 1em;
  &:hover ${TicketIcon} {
    fill: var(--icon-color);
  }
`;

interface Props {}

export function TourPage(props: Props) {
  return (
    <Root>
      <Header>Upcoming shows</Header>
      <ShowList>
        {Object.entries(SHOWS).map((entry, i) => {
          const date = entry[0];
          const location = entry[1];
          return (
            <ShowListItem key={i}>
              <Row justify="space-between">
                <DateText>{date}</DateText>
                <Row justify="flex-end">
                  <LocationText>{location}</LocationText>
                  <TicketLink>
                    <TicketIcon size="1em" paths={Icons.TICKET}></TicketIcon>
                  </TicketLink>
                </Row>
              </Row>
            </ShowListItem>
          );
        })}
      </ShowList>
    </Root>
  );
}
