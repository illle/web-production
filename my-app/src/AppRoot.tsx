import React from "react";
import "reset-css";
import styled from "styled-components";
import "./App.css";
import { DEVICES } from "./ApplicationConstants/Breakpoints";
import { Route } from "./ApplicationConstants/Route";
import { Header } from "./GUI-Components/Header";
import { NavMenu } from "./GUI-Components/NavMenu";
import { useLocalComplex } from "./Hooks/LocalStorageHooks";
import { AboutUsPage } from "./Pages/AboutUs/AboutUsPage";
import { DiscographyPage } from "./Pages/Discography/DiscographyPage";
import { TourPage } from "./Pages/Tour/TourPage";

const Root = styled.div`
  overflow: hidden;
`;

const BodyHeight = styled.div`
  max-height: calc(100vh - var(--header-height) - var(--nav-height));
  box-sizing: border-box;
  @media screen and (min-width: ${DEVICES.LANDSCAPE_TABLET}) {
    max-height: calc(100vh - var(--header-height));
  }
`;

const PageBody = styled(BodyHeight)`
  margin-top: var(--header-height);
  position: relative;
  overflow: hidden;
  @media screen and (min-width: ${DEVICES.LANDSCAPE_TABLET}) and (max-width: ${
  DEVICES.LARGE_DESKTOP
}) {
    margin-left: var(--nav-side-width);
  }
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  grid-template-rows: 100%;
  grid-template-areas: "${(props) => {
    const areas = ((props.route as string) + " ").repeat(6);
    return areas;
  }}";
  column-gap: 1em;
  color: white;
  > * {
    &:not(:last-child) {
      margin-bottom: 2em;
    }
  }
  @media only screen and (min-width: ${DEVICES.LAPTOP}) {
    grid-template-areas: "${(props) => {
      const route: Route = props.route;
      const selectedArea = ((route as string) + " ").repeat(3);
      switch (route) {
        case Route.HOME:
          return selectedArea + ((Route.LISTEN as string) + " ").repeat(3);
        case Route.LISTEN:
          return ((Route.HOME as string) + " ").repeat(3) + selectedArea;
        case Route.TOUR:
          return ((Route.HOME as string) + " ").repeat(3) + selectedArea;
      }
    }}";
    
  }
  @media only screen and (min-width: ${DEVICES.LARGE_DESKTOP}) {
    grid-template-areas: "${Route.HOME} ${Route.HOME} ${Route.TOUR} ${
  Route.TOUR
} ${Route.LISTEN} ${Route.LISTEN}";
  }
`;

const Discography = styled(BodyHeight)`
  grid-area: ${Route.LISTEN};
  display: ${(props) => (props.route === Route.LISTEN ? "inline" : "none")};
  @media only screen and (min-width: ${DEVICES.LAPTOP}) {
    display: ${(props) =>
      props.route === Route.LISTEN || props.route === Route.HOME
        ? "inline"
        : "none"};
  }
  @media only screen and (min-width: ${DEVICES.LARGE_DESKTOP}) {
    display: inline;
  }
`;

const AboutUs = styled(BodyHeight)`
  grid-area: ${Route.HOME};
  display: ${(props) => (props.route === Route.HOME ? "inline" : "none")};
  @media only screen and (min-width: ${DEVICES.LAPTOP}) {
    display: inline;
  }
  @media only screen and (min-width: ${DEVICES.LARGE_DESKTOP}) {
    display: inline;
  }
`;

const TourDates = styled(BodyHeight)`
  grid-area: ${Route.TOUR};
  display: ${(props) => (props.route === Route.TOUR ? "inline" : "none")};
  @media only screen and (min-width: ${DEVICES.LAPTOP}) {
    display: ${(props) => (props.route === Route.TOUR ? "inline" : "none")};
  }
  @media only screen and (min-width: ${DEVICES.LARGE_DESKTOP}) {
    display: inline;
  }
`;

interface Props {}

export function AppRoot(props: Props) {
  const [selectedRoute, setSelectedRoute] = useLocalComplex<Route>({
    itemName: "route",
    fallback: () => Route.HOME,
    converter: {
      objToStr: (obj: Route) => obj,
      strToObj: (str: string) =>
        Object.values(Route).find((e) => (e as string) === str),
    },
  });

  return (
    <Root>
      <Header></Header>
      <NavMenu onRoute={selectedRoute} setRoute={setSelectedRoute}></NavMenu>
      <PageBody route={selectedRoute}>
        <Discography route={selectedRoute}>
          <DiscographyPage></DiscographyPage>
        </Discography>
        <AboutUs route={selectedRoute}>
          <AboutUsPage></AboutUsPage>
        </AboutUs>
        <TourDates route={selectedRoute}>
          <TourPage></TourPage>
        </TourDates>
      </PageBody>
    </Root>
  );
}
